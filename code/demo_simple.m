%### Terms of Use
%--------------
% This Hierarchical Rank Pooling software is strictly for non-commercial academic use only. 
% This Hierarchical Rank Pooling code or any modified version of it may not be used for any commercial activity, such as:
%1. Commercial production development, Commercial production design, design validation or design assessment work.
%2. Commercial manufacturing engineering work
%3. Commercial research.
%4. Consulting work performed by academic students, faculty or academic account staff
%5. Training of commercial company employees.

%### License
%-------
% The analysis work performed with the program(s) must be non-proprietary work. 
% Licensee and its contract users must be or be affiliated with an academic facility. 
% Licensee may additionally permit individuals who are students at such academic facility 
% to access and use the program(s). Such students will be considered contract users of licensee. 
% The program(s) may not be used for commercial competitive analysis 
% (such as benchmarking) or for any commercial activity, including consulting.

% *************************************************************************
% Dependency : 
% 1. Lib Linear 
%       https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/multicore-liblinear/
% 2. VL-Feat 
%       http://www.vlfeat.org/
%
% Author : Basura Fernando (basura.fernando@anu.edu.au)

CVAL    = 1;    % The defaul C value for SVR
WIN     = 20;   % The window size of the poooling layers
STRIDE  = 1;    % The stride of the pooling layer
% define the network architecture
net = defineNetwork(CVAL,WIN,STRIDE);   

% create a sequence of 100 vectors of dimensionality 1024
sequence_data = randn(100,1024);
% get the encoding of the sequence
W = passNetwork(sequence_data,net)  ;
