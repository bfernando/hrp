%### Terms of Use
%--------------
% This Hierarchical Rank Pooling software is strictly for non-commercial academic use only. 
% This Hierarchical Rank Pooling code or any modified version of it may not be used for any commercial activity, such as:
%1. Commercial production development, Commercial production design, design validation or design assessment work.
%2. Commercial manufacturing engineering work
%3. Commercial research.
%4. Consulting work performed by academic students, faculty or academic account staff
%5. Training of commercial company employees.

%### License
%-------
% The analysis work performed with the program(s) must be non-proprietary work. 
% Licensee and its contract users must be or be affiliated with an academic facility. 
% Licensee may additionally permit individuals who are students at such academic facility 
% to access and use the program(s). Such students will be considered contract users of licensee. 
% The program(s) may not be used for commercial competitive analysis 
% (such as benchmarking) or for any commercial activity, including consulting.

% *************************************************************************
% Dependency : Lib Linear 
% Author : Basura Fernando (basura.fernando@anu.edu.au)
function net = defineNetwork(CVAL,WIN,STRIDE)     
    
    net = cell(1,1);    
    
    layer = 1;
    net{layer}.Window_size = WIN;
    net{layer}.Stride = STRIDE;
    net{layer}.poolType = 'classical';  %options : svr, normalFov, classical
    net{layer}.normalization = 'None';  %options : None, L2, SSRL2, rootL2
    net{layer}.nonlinear = ''; % The default non-linear function is SER
    net{layer}.CVAL = CVAL;
    
    % Here you can add more layers. Just copy layer 1 and change the ID.
    
%     layer = layer + 1;
%     net{layer}.Window_size = WIN;
%     net{layer}.Stride = STRIDE;
%     net{layer}.poolType = 'classical';  %options : svr, normalFov, classical
%     net{layer}.normalization = 'None';  %options : None, L2, SSRL2, rootL2
%     net{layer}.nonlinear = ''; % The default non-linear function is SER
%     net{layer}.CVAL = CVAL;
    
    % The final rank pooling layer
    layer = layer + 1;
    net{layer}.Window_size = -1;
    net{layer}.Stride = -1;
    net{layer}.poolType = 'classical'; %svr normalFov classical
    net{layer}.normalization = 'None'; % None L2
    net{layer}.nonlinear = ''; % The default non-linear function is SER
    net{layer}.CVAL = CVAL;
    
    

    
end
