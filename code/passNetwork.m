%### Terms of Use
%--------------
% This Hierarchical Rank Pooling software is strictly for non-commercial academic use only. 
% This Hierarchical Rank Pooling code or any modified version of it may not be used for any commercial activity, such as:
%1. Commercial production development, Commercial production design, design validation or design assessment work.
%2. Commercial manufacturing engineering work
%3. Commercial research.
%4. Consulting work performed by academic students, faculty or academic account staff
%5. Training of commercial company employees.

%### License
%-------
% The analysis work performed with the program(s) must be non-proprietary work. 
% Licensee and its contract users must be or be affiliated with an academic facility. 
% Licensee may additionally permit individuals who are students at such academic facility 
% to access and use the program(s). Such students will be considered contract users of licensee. 
% The program(s) may not be used for commercial competitive analysis 
% (such as benchmarking) or for any commercial activity, including consulting.

% *************************************************************************
% Dependency : Lib Linear 
% Author : Basura Fernando (basura.fernando@anu.edu.au)
function W = passNetwork(data,net,isNotsaveSequences)  
    if nargin < 3
        isNotsaveSequences = 1;
    end    
    if size(net,2) == 1
        switch net{1}.poolType
            case 'classical'
                W = genRepresentation(data,net{1}.CVAL,net{1}.nonlinear);
            case 'normalFov'
               W = genFowRepresentation(data,net{1}.CVAL,net{1}.nonlinear); 			
            case 'svr'
               W = liblinearsvr(data,net{1}.CVAL,2); 			   
        end   
        return
    end
    nFrms = size(data,1);
    LIMIT  = net{1}.Window_size;
    if nFrms < LIMIT
        LIMIT = LIMIT * 2;
        nFrms = size(data,1);
        xqv = 1:nFrms/LIMIT:nFrms;
        data = interp1(1:nFrms,data,xqv);
    end

    for layer = 1 : size(net,2)-1
        Window_size = net{layer}.Window_size;
        Stride      = net{layer}.Stride;
        poolType    = net{layer}.poolType;
        normalization = net{layer}.normalization;
        nonlinear = net{layer}.nonlinear;
        CVAL = net{layer}.CVAL;
        if layer == 1
            net{layer}.data = one_layer_darwin(data,Window_size,Stride,poolType,normalization,CVAL,nonlinear);
        else
            net{layer}.data = one_layer_darwin(net{layer-1}.data,Window_size,Stride,poolType,normalization,CVAL,nonlinear);
        end        
    end
    CVAL = net{layer}.CVAL;   
    if isNotsaveSequences == 1
        switch net{end}.poolType
            case 'classical'
                W = genRepresentation(net{end-1}.data,CVAL,net{end}.nonlinear);
            case 'normalFov'
               W = genFowRepresentation(net{end-1}.data,CVAL,net{end}.nonlinear); 			
            case 'svr'
               W = liblinearsvr(net{end-1}.data,CVAL,2);       		
        end   
    else
        W = net{end-1}.data;
    end
end


function out = one_layer_darwin(data,Window_size,Stride,poolType,normalization,CVAL,nonlinear)
    n = size(data,1);
    Dim = size(data,2)*2;
    fstart = 1:Stride:(n-Window_size+1);
    fend = fstart + Window_size -1;    
    switch poolType
        case 'classical'
            out = zeros(numel(fstart),Dim*2);
        case 'normalFov'   
            out = zeros(numel(fstart),Dim);
         case 'svr'    
            out = zeros(numel(fstart),Dim); 
         case 'max'    
            out = zeros(numel(fstart),Dim);    
          
    end
    %parfor chunk = 1 : numel(fstart)
    for chunk = 1 : numel(fstart)
        data_chuck = data(fstart(chunk):fend(chunk),:);
        switch poolType
            case 'classical'
                out(chunk,:) = genRepresentation(data_chuck,CVAL,nonlinear);
            case 'normalFov'
               out(chunk,:) = genFowRepresentation(data_chuck,CVAL,nonlinear); 			
            case 'svr'
               out(chunk,:) = liblinearsvr(data_chuck,CVAL,2); 
            case 'max'
               out(chunk,:) = max(data_chuck);         
        end              
    end
    %fprintf('\n');
    switch normalization
            case 'L2'
                out = normalizeL2(out);
            case 'None'
                
            case 'rootL2'                
                out = normalizeL2(sqrt(out));
            case 'SSRL2'                
                out = normalizeL2(sign(out).*sqrt(abs(out)));    
    end
end

